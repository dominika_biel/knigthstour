package pl.bield

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


class WorldSpec {

    @Test
    fun whenThereIsOnePossibleNextDestinationItIsChosen(): Unit {
        //given we're start point and there is one possible direction
        val startPosition = Square('A', 1)
        val world = World(startPosition, boundaries = Boundaries('A', 'B', 1, 3))
        //when we're doing a move
        val nextStep = world.makeMove()

        //then we get a new world where we're in the destination
        assertThat(world.position).isEqualTo(startPosition)
        assertThat(world.checkPossibleNextPlacesHistoryAware()).hasSize(1)
        assertThat(nextStep.position).isEqualTo(Square('B', 3))
    }

    @Test
    fun whenThereIsMultiplePossibleNextDestinationsOneOfThemIsChosen(): Unit {
        //given we're start point and there is multiple possible direction
        val startPosition = Square('C', 3)
        val world = World(startPosition)
        //when we're doing a move
        val next = world.makeMove()
        //then we get a new step where we're in the destination
        assertThat(world.position).isEqualTo(startPosition)
        assertThat(next.position).isIn(world.checkPossibleNextPlacesHistoryAware())
    }


    @Test
    fun justVisitedPositionsAreNotPossibleTargets() {
        //given a step and its predecessor
        val predecessorStep = World(Square('A', 2))
        val currentStep = predecessorStep.makeMove()
        //when finding next steps
        val possibleSteps = currentStep.checkPossibleNextPlacesHistoryAware()
        //then current step and predecessor coordinates are excluded
        assertThat(possibleSteps).doesNotContain(predecessorStep.position, currentStep.position)
    }

    @Test
    fun anyVisitedPositionsAreNotPossibleTargets() {
        //given a step and its predecessor
        val currentStep = Step(Square('D', 3))
        val positions = currentStep.possibleNextPlaces
        val prepredecessorStep = Step(positions.first())
        val predecessorStep = Step(positions.last())
        //when finding next steps
        val currentWorld = World(currentStep.position, history = listOf(prepredecessorStep, predecessorStep))
        val possibleSteps = currentWorld.checkPossibleNextPlacesHistoryAware()
        //then current step and predecessor coordinates are excluded
        assertThat(possibleSteps)
                .doesNotContain(prepredecessorStep.position, predecessorStep.position, currentWorld.position)
    }



    @Test
    fun worldShouldContainFirstStepInPath() {
        //given there were at least 1 step performed
        val startPosition = Square('A', 1)
        val firstStep = Step(startPosition)
        val world: World = World(startPosition, Boundaries())
        //when we check current world state
        val state = world.path
        //then current world shows path of the knight which includes the 1st step
        assertThat(state).asList().contains(firstStep)
    }

    @Test
    fun stepsShouldCreatePath() {
        //given there were some steps performed
        val startPosition = Square('A', 1)
        val firstWorld: World = World(startPosition, Boundaries())
        val currentWorld: World = firstWorld.makeMove()
        //when we check current world state
        val state = currentWorld.path
        //then current world shows path of the knight
        assertThat(state).asList().contains(Step(startPosition))
        assertThat(state).hasSize(2)
    }

    @Test
    fun whenThereAreNoPossibleMovesShouldTryToBackAway() {
        //given
        val deadEnd = Square('B', 2)
        val newHope = Square('C', 2)
        val stepWith2Options = Step(Square('A', 1))
        stepWith2Options.possibleNextPlaces = listOf(deadEnd, newHope)
        val stepWithNoOptions = Step(deadEnd)
        stepWithNoOptions.possibleNextPlaces = emptyList()
        val worldAtEnd = World(deadEnd, history = listOf(stepWith2Options), currentStep = stepWithNoOptions)
        //when
        val apocalypse = worldAtEnd.makeMove().makeMove()
        //then
        assertThat(apocalypse.position).isEqualTo(newHope)
    }

    @Test
    fun backAwayShortensHistory() {
        //given a world with history
        val steps = listOf(Step(Square('A', 2)), Step(Square('A', 4)))
        val world = World(Square('C', 3), history = steps)
        //when back away is called
        val backed = world.backAway()
        //then history is reduced
        assertThat(backed.history).asList().containsExactly(Step(Square('A', 2)))
    }

    @Test
    fun backAwayGoesBackAPosition() {
        //given a world with history
        val steps = listOf(Step(Square('A', 2)), Step(Square('A', 4)))
        val world = World(Square('C', 3), history = steps)
        //when back away is called
        val backed = world.backAway()
        //then position is first from history
        assertThat(backed.position).isEqualTo(Square('A', 4))
    }

    @Test
    fun backAwayGoesBackAStep() {
        //given a world with history
        val steps = listOf(Step(Square('A', 2)), Step(Square('A', 4)))
        val world = World(Square('C', 3), history = steps)
        //when back away is called
        val backed = world.backAway()
        //then history is reduced
        assertThat(backed.currentStep).isEqualTo(Step(Square('A', 4)))
    }

    @Test
    fun backAwayShortensPath() {
        //given a world with history
        val steps = listOf(Step(Square('A', 2)), Step(Square('A', 4)))
        val world = World(Square('C', 3), history = steps)
        //when back away is called
        assertThat(world.path).asList().isEqualTo(steps.plus(world.currentStep))
        val backed = world.backAway()
        //then path is reduced
        assertThat(backed.path).asList().isEqualTo(steps)
    }

    @Test
    fun fullPathHasAllBoard() {
        //given a board that can be marked fully
        val startWorld = World(position = Square('C', 3), boundaries = Boundaries(maxX = 'E', maxY = 5))
        //when there is no more moves
        var endWorld = startWorld
        while (!endWorld.isDone()) {
            endWorld = endWorld.makeMove()
        }
        //then path size is same as number of squares in board (board is fully marked)
        assertThat(endWorld.path)
                .hasSize(startWorld.boardSize())
                .doesNotHaveDuplicates()
    }

    @Test
    fun fdsvfxcbgfbnbhvc() {
        //given a board that can be marked fully
        val startPosition = Square('A', 1)
        val startWorld = World(position = startPosition, boundaries = Boundaries(maxX = 'C', maxY = 3))
        //when there is no more moves
        var endWorld = startWorld
        while (!endWorld.isDone()) {
            endWorld = endWorld.makeMove()
        }
        //then path size is same as number of squares in board (board is fully marked)
        assertThat(endWorld.path).containsExactly(Step(position = startPosition))
    }

    @Test
    fun sizeFor5x5BoardShouldBe25() {
        val boundaries = Boundaries(maxX = 'E', maxY = 5)
        val size = boundaries.boardSize()
        assertThat(size).isEqualTo(25)
    }
}





