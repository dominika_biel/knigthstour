package pl.bield

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class StepSpec {

    @Test
    fun possibleMovesFromMiddleOfTheBoard() {
        //given the knight is in the middle of the board and it's first move
        val currentStep = Step(Square('C', 3))
        val nextChoices = listOf(Square('A', 2), Square('A', 4), Square('B', 1), Square('B', 5),
                Square('D', 1), Square('D', 5), Square('E', 2), Square('E', 4))
        //when check for possible next places
        val choices: Collection<Square> = currentStep.possibleNextPlaces
        //then there is 8 possible choices
        assertThat(choices).asList().containsExactlyElementsOf(nextChoices)
    }


    @Test //todo remove
    fun possibleMovesFromMiddleOfTheBoard2() {
        //given the knight is in the middle of the board and it's first move
        val currentStep = Step(Square('D', 3))
        val nextChoices = listOf(Square('B', 2), Square('B', 4), Square('C', 1), Square('C', 5),
                Square('E', 1), Square('E', 5), Square('F', 2), Square('F', 4))
        //when check for possible next places
        val choices: Collection<Square> = currentStep.possibleNextPlaces
        //then there is 8 possible choices
        assertThat(choices).asList().containsExactlyElementsOf(nextChoices)
    }

    @Test
    fun possibleMovesShouldBeRestrictedByBoardBoundaries() {
        //given the knight is in near board's corner
        val currentStep = Step(Square('A', 1))
        val nextChoices = listOf(Square('B', 3), Square('C', 2))
        //when check for possible next places
        val choices: Collection<Square> = currentStep.possibleNextPlaces
        //then there is less than 8 possible choices
        assertThat(choices).asList().containsExactlyElementsOf(nextChoices)
    }
}

