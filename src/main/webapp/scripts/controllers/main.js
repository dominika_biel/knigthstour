'use strict';

/**
 * @ngdoc function
 * @name knightstourApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the knightstourApp
 */
angular.module('knightstourApp')
  .controller('MainCtrl', function ($http, $httpParamSerializer, $interval) {
    var scope = this;
    scope.board = {};

    function initBoard() {
      $http.get("/create?" + $httpParamSerializer({x: 'C', y: 4})).then(function (data) {
        scope.world = data;
        scope.makeBoard();
      });
    }

    initBoard();

    this.next = function () {
      $http.get("/next").then(function (data) {
        scope.world = data;
      });
      scope.makeBoard();
    };

    this.play = function () {
      scope.playing = $interval(scope.next, 100);
    };

    this.stop = function () {
      $interval.cancel(scope.playing);
      scope.playing = null;
    };

    this.canStop = function () {
      return !!scope.playing;
    };

    this.makeBoard = function () {
      var minX = 'A', maxX = 'H';
      var minY = 1, maxY = 8;
      var board = {};
      for (var x = minX.charCodeAt(0); x <= maxX.charCodeAt(0); x++) {
        var xx = String.fromCharCode(x);
        board[xx] = {};
        for (var y = minY; y <= maxY; y++) {
          board[xx][y] = {visited: false};
        }
      }
      scope.board = board;
      return scope.fillBoard(board);
    };

    this.fillBoard = function (board) {
      var path = scope.world.data.path;
      for (var i = 0; i < path.length; i++) {
        var step = path[i];
        var x = step.position.xcoordinate;
        var y = step.position.ycoordinate;
        board[x][y].visited = true;
      }
      return board;
    }
  });
