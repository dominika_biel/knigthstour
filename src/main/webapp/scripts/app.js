'use strict';

/**
 * @ngdoc overview
 * @name knightstourApp
 * @description
 * # knightstourApp
 *
 * Main module of the application.
 */
angular
  .module('knightstourApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
