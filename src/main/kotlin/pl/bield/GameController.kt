package pl.bield

import org.springframework.context.annotation.Scope
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController @Scope("session")
class GameController {

    var world: World? = null

    @RequestMapping("/create")
    fun createWorld(@RequestParam x: Char,
                    @RequestParam y: Int,
                    @RequestParam(defaultValue = "A") minX: Char,
                    @RequestParam(defaultValue = "H") maxX: Char,
                    @RequestParam(defaultValue = "1") minY: Int,
                    @RequestParam(defaultValue = "8") maxY: Int): World {
        val position = Square(x, y)
        val boundaries = Boundaries(minX, maxX, minY, maxY)
        world = World(position = position, boundaries = boundaries)
        return world!!
    }


    @RequestMapping("/next")
    fun next(): World? {
            world = world?.makeMove()
            return world
    }

}


