package pl.bield

data class Square(
        val xCoordinate: Char,
        val yCoordinate: Int) {
    constructor(old: Square, shiftX: Int, shiftY: Int) : this(old.xCoordinate + shiftX, old.yCoordinate + shiftY)
}