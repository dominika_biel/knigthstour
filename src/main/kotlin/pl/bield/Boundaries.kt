package pl.bield

data class Boundaries(
        val minX: Char = 'A',
        val maxX: Char = 'H',
        val minY: Int = 1,
        val maxY: Int = 8) {
    fun boardSize(): Int {
        return ((maxX - minX + 1) * (maxY - minY + 1))
    }
}