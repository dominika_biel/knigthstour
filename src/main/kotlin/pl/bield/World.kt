package pl.bield

data class World(
        val position: Square,
        val boundaries: Boundaries = Boundaries(),
        val history: List<Step> = emptyList(),
        val currentStep: Step = Step(position, boundaries)) {

    val path: List<Step>

    init {
        path = history.plus(currentStep)
    }

    fun makeMove(): World {
        val possibleNextPlaces = checkPossibleNextPlacesHistoryAware()
        if (possibleNextPlaces.isNotEmpty()) {
            return World(
                    position = possibleNextPlaces.last(),
                    history = path,
                    boundaries = boundaries)
        } else {
            return backAway()
        }
    }

    fun backAway(): World {
        val currentStep = history.last()

        val massacredHistory = history.minus(currentStep)
        currentStep.possibleNextPlaces = currentStep.possibleNextPlaces.minus(this.position)
        return World(
                currentStep = currentStep,
                position = currentStep.position,
                boundaries = boundaries,
                history = massacredHistory)
    }

    fun isDone(): Boolean {
        val allSquaresVisited = path.size == boardSize()
        val noMoreMoves = history.isEmpty() && checkPossibleNextPlacesHistoryAware().size == 0
        return allSquaresVisited || noMoreMoves
    }

    fun boardSize(): Int {
        return boundaries.boardSize()
    }

    fun checkPossibleNextPlacesHistoryAware(): Collection<Square> {
        return currentStep.possibleNextPlaces.minus(allPreviousPositions(path))
    }

    private fun allPreviousPositions(path: List<Step>): Collection<Square> {
        return path.map { it.position }
    }
}