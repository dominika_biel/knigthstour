package pl.bield

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.web.SpringBootServletInitializer
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@SpringBootApplication
open class DemoApplication : SpringBootServletInitializer() {
    override fun configure(application: SpringApplicationBuilder?): SpringApplicationBuilder? {
        return application?.sources(DemoApplication::class.java)
    }
}

@RequestMapping("/")
@Controller
open class WebConfig{


    @RequestMapping
    fun root():String{
        return "redirect:/index.html"
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(DemoApplication::class.java, *args)
}
