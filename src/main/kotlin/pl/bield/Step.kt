package pl.bield

data class Step private constructor(val position: Square) {
    var possibleNextPlaces: Collection<Square> = emptyList()

    constructor(position: Square, possiblePlaces: Collection<Square>) : this(position) {
        this.possibleNextPlaces = possiblePlaces
    }

    constructor(position: Square, boundaries: Boundaries = Boundaries()) : this(position) {
        this.possibleNextPlaces = findPossibleNextPlaces(boundaries)
    }


    private fun findPossibleNextPlaces(boundaries: Boundaries): Collection<Square> {
        val position = position
        return squaresInKnightRange(position)
                .filter(squareInsideBoundaries(boundaries))
    }

    private fun squaresInKnightRange(position: Square): List<Square> {
        return listOf(
                Square(position, -2, -1), Square(position, -2, +1),
                Square(position, -1, -2), Square(position, -1, +2),
                Square(position, +1, -2), Square(position, +1, +2),
                Square(position, +2, -1), Square(position, +2, +1))
    }

    private fun squareInsideBoundaries(boundaries: Boundaries): (Square) -> Boolean {
        return {
            it.xCoordinate >= boundaries.minX && it.xCoordinate <= boundaries.maxX
                    && it.yCoordinate >= boundaries.minY && it.yCoordinate <= boundaries.maxY
        }
    }
}